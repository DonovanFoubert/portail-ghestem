<?php

namespace App\Http\Controllers;

use App\Message;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    protected $messageRepository;

    protected $nbrPerPage = 4;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $message = $this->messageRepository->getPaginate($this->nbrPerPage);
        return view('messages', compact('message'));
    }

    public function listeForSite()
    {
        return $this->liste(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["type"])){ $newData["type_id"] = $request->newData["type"]; unset($newData["type"]); }
        $this->messageRepository->store($newData);
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**edit
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["type"])){ $newData["type_id"] = $request->newData["type"]; unset($newData["type"]); }

        $message = $this->messageRepository->find($request["id"]);
        $message->update($newData);
        $message->save();

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->guest()){
            return false;
        }

        $this->messageRepository->destroy($id);

        return true;
    }
}
