<?php

namespace App\Http\Controllers;

use App\MessageType;
use App\Repositories\MessageTypeRepository;
use Illuminate\Http\Request;

class MessageTypeController extends Controller
{
    protected $messageTypeRepository;

    protected $nbrPerPage = 4;

    public function __construct(MessageTypeRepository $messageTypeRepository)
    {
        $this->messageTypeRepository = $messageTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $messageTypes = $this->messageTypeRepository->all();

        foreach($messageTypes as $type){
            $arrayReturn[] = ["id" => $type->id, "type" => $type->type];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MessageType $messageType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(MessageType $messageType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MessageType $messageType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MessageType $messageType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }
}
