<?php

namespace App\Http\Controllers;

use App\Order;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $orderRepository;

    protected $nbrPerPage = 4;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $order = $this->orderRepository->getPaginate($this->nbrPerPage);
        return view('orders', compact('order'));
    }

    /**
     * Display order detail.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $order = $this->orderRepository->find($id);
        if(empty($order)){
            return back()->with('warning', "Cette commande n'existe pas");
        }

        $messages = json_encode($order->messages);
        return view('orders', compact('order','messages'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function messages($id)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $order = $this->orderRepository->find($id);
        if(empty($order)){
            return back()->with('warning', "Cette commande n'existe pas");
        }

        return view('orders_messages', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $orders = $this->orderRepository->all();

        foreach($orders as $order){
            $arrayReturn[] = ["id"              =>$order->id,
                "name"          =>$order->name,
                "address"       =>$order->address,
                "postal_code"   =>$order->postal_code,
                "city"          =>$order->city,
                "type"          =>$order->type->id,
                "status"        =>$order->status->id,
                "country"       =>$order->country];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listeProducts($id)
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];
        $order = $this->orderRepository->find($id);

        if(empty($order)){
            return back()->with('warning', "Cette commande n'existe pas");
        }

        //dd($order->products);

        foreach($order->products as $product){
            $arrayReturn[] = ["reference"       =>$product->reference,
                                "description"   =>$product->description,
                                "quantity"      =>$product->pivot->quantity,
                                "observation"   =>$product->pivot->observation];
        }

        return json_encode($arrayReturn);
    }

    public function listeForSite()
    {
        return $this->liste(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function new()
    {
        if(auth()->guest()){
            return false;
        }

        return view('/order');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["type"])){ $newData["type_id"] = $request->newData["type"]; unset($newData["type"]); }
        $this->orderRepository->store($newData);
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Third $third)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**edit
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["type"])){ $newData["type_id"] = $request->newData["type"]; unset($newData["type"]); }

        $order = $this->orderRepository->find($request["id"]);
        $order->update($newData);
        $order->save();

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Third $third)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->guest()){
            return false;
        }

        $this->orderRepository->destroy($id);

        return true;
    }
}
