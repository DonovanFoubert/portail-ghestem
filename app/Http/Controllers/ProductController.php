<?php

namespace App\Http\Controllers;

use App\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productRepository;

    protected $nbrPerPage = 4;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $products = $this->productRepository->getPaginate($this->nbrPerPage);
        return view('products.products', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $products = $this->productRepository->all();

        foreach($products as $product){
            $arrayReturn[] = ["id"              =>$product->id,
                "ean"           =>$product->ean,
                "reference"     =>$product->reference,
                "description"   =>$product->description,
                "height"        =>$product->height,
                "length"        =>$product->length,
                "width"         =>$product->width,
                "weight"        =>$product->weight,
                "stackable"     =>$product->stackable,
                "carrier"       =>$product->carrier,
                "flatbed"       =>$product->flatbed,
                "third"         =>$product->third->id,
                "status"        =>$product->status->id,
                "category"      =>$product->category->id];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["category"])){ $newData["category_id"] = $request->newData["category"]; unset($newData["category"]); }
        if(isset($newData["third"])){ $newData["third_id"] = $request->newData["third"]; unset($newData["third"]); }
        if(isset($newData["stackable"]) && $newData["stackable"] == "true"){ $newData["stackable"] = 1; } else { $newData["stackable"] = 0; }
        if(isset($newData["carrier"]) && $newData["carrier"] == "true"){ $newData["carrier"] = 1; } else { $newData["carrier"] = 0; }
        if(isset($newData["flatbed"]) && $newData["flatbed"] == "true"){ $newData["flatbed"] = 1; } else { $newData["flatbed"] = 0; }
        $this->productRepository->store($newData);
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["category"])){ $newData["category_id"] = $request->newData["category"]; unset($newData["category"]); }
        if(isset($newData["stackable"]) && $newData["stackable"] == "true"){ $newData["stackable"] = 1; } else { $newData["stackable"] = 0; }
        if(isset($newData["carrier"]) && $newData["carrier"] == "true"){ $newData["carrier"] = 1; } else { $newData["carrier"] = 0; }
        if(isset($newData["flatbed"]) && $newData["flatbed"] == "true"){ $newData["flatbed"] = 1; } else { $newData["flatbed"] = 0; }

        $product = $this->productRepository->find($request["id"]);
        $product->update($newData);
        $product->save();

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->guest()){
            return false;
        }

        $this->productRepository->destroy($id);

        return true;
    }
}
