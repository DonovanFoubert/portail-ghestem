<?php

namespace App\Http\Controllers;

use App\ProductStatus;
use App\Repositories\ProductStatusRepository;
use Illuminate\Http\Request;

class ProductStatusController extends Controller
{

    protected $productStatusRepository;

    protected $nbrPerPage = 4;

    public function __construct(ProductStatusRepository $productStatusRepository)
    {
        $this->productStatusRepository = $productStatusRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $productsStatus = $this->productStatusRepository->all();

        foreach($productsStatus as $product){
            $arrayReturn[] = ["id" => $product->id, "status" => $product->status];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ProductStatus $productStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductStatus $productStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductStatus $productStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductStatus $productStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }
}
