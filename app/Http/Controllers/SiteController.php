<?php

namespace App\Http\Controllers;

use App\Site;
use App\Repositories\SiteRepository;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    protected $siteRepository;

    protected $nbrPerPage = 4;

    public function __construct(SiteRepository $siteRepository)
    {
        $this->siteRepository = $siteRepository;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $sites = $this->siteRepository->all();

        foreach($sites as $site){
            $arrayReturn[] = ["id"              =>$site->id,
                                "name"          =>$site->name,
                                "address"       =>$site->address,
                                "postal_code"   =>$site->postal_code,
                                "city"          =>$site->city,
                                "third"         =>$site->third->id,
                                "country"       =>$site->country];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $site = $this->siteRepository->getPaginate($this->nbrPerPage);
        return view('sites', compact('site'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["third"])){ $newData["third_id"] = $request->newData["third"]; unset($newData["third"]); }
        $this->siteRepository->store($newData);
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**edit
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["third"])){ $newData["third_id"] = $request->newData["third"]; unset($newData["third"]); }

        $site = $this->siteRepository->find($request["id"]);
        $site->update($newData);
        $site->save();

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Third $third)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->guest()){
            return false;
        }

        $this->siteRepository->destroy($id);

        return true;
    }
}
