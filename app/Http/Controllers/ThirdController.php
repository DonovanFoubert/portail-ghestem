<?php

namespace App\Http\Controllers;

use App\Third;
use App\Repositories\ThirdRepository;
use Illuminate\Http\Request;

class ThirdController extends Controller
{
    protected $thirdRepository;

    protected $nbrPerPage = 4;

    public function __construct(ThirdRepository $thirdRepository)
    {
        $this->thirdRepository = $thirdRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
        $third = $this->thirdRepository->getPaginate($this->nbrPerPage);
        return view('thirds', compact('third'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste(bool $flagSite = false)
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        if(!$flagSite){
            $thirds = $this->thirdRepository->allClients();
        } else {
            $thirds = $this->thirdRepository->all();
        }

        foreach($thirds as $third){
            $arrayReturn[] = ["id"              =>$third->id,
                "name"          =>$third->name,
                "address"       =>$third->address,
                "postal_code"   =>$third->postal_code,
                "city"          =>$third->city,
                "type"          =>$third->type->id,
                "status"        =>$third->status->id,
                "country"       =>$third->country];
        }

        return json_encode($arrayReturn);
    }

    public function listeForSite()
    {
        return $this->liste(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["type"])){ $newData["type_id"] = $request->newData["type"]; unset($newData["type"]); }
        $this->thirdRepository->store($newData);
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Third $third)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**edit
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(auth()->guest()){
            return false;
        }

        $newData=$request->newData;

        if(isset($newData["status"])){ $newData["status_id"] = $request->newData["status"]; unset($newData["status"]); }
        if(isset($newData["type"])){ $newData["type_id"] = $request->newData["type"]; unset($newData["type"]); }

        $third = $this->thirdRepository->find($request["id"]);
        $third->update($newData);
        $third->save();

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Third $third)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->guest()){
            return false;
        }

        $this->thirdRepository->destroy($id);

        return true;
    }
}
