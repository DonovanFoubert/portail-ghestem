<?php

namespace App\Http\Controllers;

use App\ThirdStatus;
use App\Repositories\ThirdStatusRepository;
use Illuminate\Http\Request;

class ThirdStatusController extends Controller
{

    protected $thirdStatusRepository;

    protected $nbrPerPage = 4;

    public function __construct(ThirdStatusRepository $thirdStatusRepository)
    {
        $this->thirdStatusRepository = $thirdStatusRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $thirdStatus = $this->thirdStatusRepository->all();

        foreach($thirdStatus as $third){
            $arrayReturn[] = ["id" => $third->id, "status" => $third->status];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ThirdStatus $thirdStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(ThirdStatus $thirdStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ThirdStatus $thirdStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductStatus  $productStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ThirdStatus $thirdStatus)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }
}
