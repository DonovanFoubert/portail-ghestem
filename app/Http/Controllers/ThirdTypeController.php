<?php

namespace App\Http\Controllers;

use App\ThirdType;
use App\Repositories\ThirdTypeRepository;
use Illuminate\Http\Request;

class ThirdTypeController extends Controller
{
    protected $thirdTypeRepository;

    protected $nbrPerPage = 4;

    public function __construct(ThirdTypeRepository $thirdTypeRepository)
    {
        $this->thirdTypeRepository = $thirdTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liste()
    {
        if(auth()->guest()){
            return false;
        }

        $arrayReturn = [];

        $thirdTypes = $this->thirdTypeRepository->all();

        foreach($thirdTypes as $type){
            $arrayReturn[] = ["id" => $type->id, "type" => $type->type];
        }

        return json_encode($arrayReturn);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ThirdType $thirdType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ThirdType $thirdType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ThirdType $thirdType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ThirdType $thirdType)
    {
        if(auth()->guest()){
            return view('auth.login');
        }
    }
}
