<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'message';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message', 'user_id', 'status_id', 'type_id', 'order_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function type()
    {
        return $this->belongsTo('App\MessageType');
    }

    public function status()
    {
        return $this->belongsTo('App\OrderStatus');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
