<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageType extends Model
{
    protected $table = 'message_type';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type'
    ];

    public function messages()
    {
        return $this->hasMany('App\Message');
    }
}
