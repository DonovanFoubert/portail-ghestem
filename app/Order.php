<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference', 'third_id', 'type_id', 'volume', 'weight', 'ad_valorem', 'ot', 'contract_week',
        'wish_week', 'year', 'delivery_date', 'status', 'note', 'price', 'country_price', 'island_price', 'special_price',
        'against_cash', 'departure_site_id', 'destination_site_id'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity', 'observation');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function type()
    {
        return $this->belongsTo('App\OrderType');
    }

    public function status()
    {
        return $this->belongsTo('App\OrderStatus');
    }

    public function departure_site()
    {
        return $this->belongsTo('App\Site', 'departure_site_id');
    }

    public function destination_site()
    {
        return $this->belongsTo('App\Site', 'destination_site_id');
    }

    public function third()
    {
        return $this->belongsTo('App\Third');
    }
}
