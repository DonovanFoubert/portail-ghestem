<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ean', 'reference', 'description', 'height', 'length', 'width', 'weight', 'stackable', 'category_id', 'status_id',
        'carrier', 'flatbed', 'third_id'
    ];

    public function third()
    {
        return $this->belongsTo('App\Third');
    }

    public function category()
    {
        return $this->belongsTo('App\ProductCategory');
    }

    public function status()
    {
        return $this->belongsTo('App\ProductStatus');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
