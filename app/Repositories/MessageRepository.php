<?php

namespace App\Repositories;

use App\Message;

class MessageRepository
{

    protected $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function find($id){
        return $this->message->find($id);
    }

    public function all(){
        return $this->message->all();
    }

    public function getPaginate($n)
    {
        return $this->message
            ->orderBy('order', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->message->create($inputs);
    }

    public function destroy($id)
    {
        $this->message->findOrFail($id)->delete();
    }

}
