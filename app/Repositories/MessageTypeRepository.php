<?php

namespace App\Repositories;

use App\MessageType;

class MessageTypeRepository
{

    protected $messageType;

    public function __construct(MessageType $messageType)
    {
        $this->messageType = $messageType;
    }

    public function find($id){
        return $this->messageType->find($id);
    }

    public function all(){
        return $this->messageType->all();
    }

    public function getPaginate($n)
    {
        return $this->messageType
            ->orderBy('id', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->messageType->create($inputs);
    }

    public function destroy($id)
    {
        $this->messageType->findOrFail($id)->delete();
    }

}
