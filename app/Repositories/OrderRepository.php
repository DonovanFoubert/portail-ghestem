<?php

namespace App\Repositories;

use App\Order;

class OrderRepository
{

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function find($id){
        return $this->order->find($id);
    }

    public function all(){
        return $this->order->all();
    }

    public function allClients(){
        return $this->order->where("type_id", "=", "2")->get();
    }

    public function getPaginate($n)
    {
        return $this->order
            ->orderBy('id', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->order->create($inputs);
    }

    public function destroy($id)
    {
        $this->order->findOrFail($id)->delete();
    }

}
