<?php

namespace App\Repositories;

use App\OrderStatus;

class OrderStatusRepository
{

    protected $orderStatus;

    public function __construct(OrderStatus $orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    public function find($id){
        return $this->orderStatus->find($id);
    }

    public function all(){
        return $this->orderStatus->all();
    }

    public function getPaginate($n)
    {
        return $this->orderStatus
            ->orderBy('status', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->orderStatus->create($inputs);
    }

    public function destroy($id)
    {
        $this->orderStatus->findOrFail($id)->delete();
    }

}
