<?php

namespace App\Repositories;

use App\OrderType;

class OrderTypeRepository
{

    protected $orderType;

    public function __construct(OrderType $orderType)
    {
        $this->orderType = $orderType;
    }

    public function find($id){
        return $this->orderType->find($id);
    }

    public function all(){
        return $this->orderType->all();
    }

    public function getPaginate($n)
    {
        return $this->orderType
            ->orderBy('id', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->orderType->create($inputs);
    }

    public function destroy($id)
    {
        $this->orderType->findOrFail($id)->delete();
    }

}
