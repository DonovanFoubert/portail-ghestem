<?php

namespace App\Repositories;

use App\ProductCategory;

class ProductCategoryRepository
{

    protected $productCategory;

    public function __construct(ProductCategory $productCategory)
    {
        $this->productCategory = $productCategory;
    }

    public function find($id){
        return $this->productCategory->find($id);
    }

    public function all(){
        return $this->productCategory->all();
    }

    public function getPaginate($n)
    {
        return $this->productCategory
            ->orderBy('id', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->productCategory->create($inputs);
    }

    public function destroy($id)
    {
        $this->productCategory->findOrFail($id)->delete();
    }

}
