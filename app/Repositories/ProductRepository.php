<?php

namespace App\Repositories;

use App\Product;

class ProductRepository
{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function find($id){
        return $this->product->find($id);
    }

    public function all(){
        return $this->product->all();
    }

    public function getPaginate($n)
    {
        return $this->product
            ->orderBy('product.ean', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->product->create($inputs);
    }

    public function destroy($id)
    {
        $this->product->findOrFail($id)->delete();
    }

}
