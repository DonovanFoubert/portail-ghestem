<?php

namespace App\Repositories;

use App\ProductStatus;

class ProductStatusRepository
{

    protected $productStatus;

    public function __construct(ProductStatus $productStatus)
    {
        $this->productStatus = $productStatus;
    }

    public function find($id){
        return $this->productStatus->find($id);
    }

    public function all(){
        return $this->productStatus->all();
    }

    public function getPaginate($n)
    {
        return $this->productStatus
            ->orderBy('id', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->productStatus->create($inputs);
    }

    public function destroy($id)
    {
        $this->productStatus->findOrFail($id)->delete();
    }

}
