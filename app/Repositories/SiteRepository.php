<?php

namespace App\Repositories;

use App\Site;

class SiteRepository
{

    protected $site;

    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    public function find($id){
        return $this->site->find($id);
    }

    public function all(){
        return $this->site->all();
    }

    public function getPaginate($n)
    {
        return $this->site
            ->orderBy('name', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->site->create($inputs);
    }

    public function destroy($id)
    {
        $this->site->findOrFail($id)->delete();
    }

}
