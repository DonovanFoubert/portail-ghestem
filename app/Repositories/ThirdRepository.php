<?php

namespace App\Repositories;

use App\Third;

class ThirdRepository
{

    protected $third;

    public function __construct(Third $third)
    {
        $this->third = $third;
    }

    public function find($id){
        return $this->third->find($id);
    }

    public function all(){
        return $this->third->all();
    }

    public function allClients(){
        return $this->third->where("type_id", "=", "2")->get();
    }

    public function getPaginate($n)
    {
        return $this->third
            ->orderBy('name', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->third->create($inputs);
    }

    public function destroy($id)
    {
        $this->third->findOrFail($id)->delete();
    }

}
