<?php

namespace App\Repositories;

use App\ThirdStatus;

class ThirdStatusRepository
{

    protected $thirdStatus;

    public function __construct(ThirdStatus $thirdStatus)
    {
        $this->thirdStatus = $thirdStatus;
    }

    public function find($id){
        return $this->thirdStatus->find($id);
    }

    public function all(){
        return $this->thirdStatus->all();
    }

    public function getPaginate($n)
    {
        return $this->companyStatus
            ->orderBy('status', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->thirdStatus->create($inputs);
    }

    public function destroy($id)
    {
        $this->thirdStatus->findOrFail($id)->delete();
    }

}
