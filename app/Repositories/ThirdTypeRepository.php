<?php

namespace App\Repositories;

use App\ThirdType;

class ThirdTypeRepository
{

    protected $thirdType;

    public function __construct(ThirdType $thirdType)
    {
        $this->thirdType = $thirdType;
    }

    public function find($id){
        return $this->thirdType->find($id);
    }

    public function all(){
        return $this->thirdType->all();
    }

    public function getPaginate($n)
    {
        return $this->thirdType
            ->orderBy('id', 'asc')
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->thirdType->create($inputs);
    }

    public function destroy($id)
    {
        $this->thirdType->findOrFail($id)->delete();
    }

}
