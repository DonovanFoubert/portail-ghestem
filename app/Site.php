<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'site';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'postal_code', 'city', 'country', 'third_id'
    ];

    public function third()
    {
        return $this->belongsTo('App\Third');
    }
}
