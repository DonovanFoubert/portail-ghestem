<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Third extends Model
{
    protected $table = 'third';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'postal_code', 'city', 'country', 'third_id'
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function type()
    {
        return $this->belongsTo('App\ThirdType');
    }

    public function status()
    {
        return $this->belongsTo('App\ThirdStatus');
    }

    public function sites()
    {
        return $this->hasMany('App\Site');
    }
}
