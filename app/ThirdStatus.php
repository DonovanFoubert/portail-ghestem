<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdStatus extends Model
{
    protected $table = 'third_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status'
    ];

    public function companies()
    {
        return $this->hasMany('App\Third');
    }
}
