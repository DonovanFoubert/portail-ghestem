<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdType extends Model
{
    protected $table = 'third_type';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type'
    ];

    public function companies()
    {
        return $this->hasMany('App\Third');
    }
}
