<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThirdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('third', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('address', 255);
            $table->string('postal_code', 50);
            $table->string('city', 50);
            $table->string('country', 50);
            $table->bigInteger('status_id')->unsigned();
            $table->bigInteger('type_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('third_status')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('type_id')->references('id')->on('third_type')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('third', function(Blueprint $table) {
            $table->dropForeign('third_status_id_foreign');
        });

        Schema::table('third', function(Blueprint $table) {
            $table->dropForeign('third_type_id_foreign');
        });

        Schema::dropIfExists('third');
    }
}
