<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('address', 255);
            $table->string('postal_code', 50);
            $table->string('city', 50);
            $table->string('country', 50);
            $table->bigInteger('third_id')->unsigned();
            $table->foreign('third_id')->references('id')->on('third')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site', function(Blueprint $table) {
            $table->dropForeign('site_third_id_foreign');
        });

        Schema::dropIfExists('site');
    }
}
