<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('ean', 13);
            $table->string('reference', 20);
            $table->string('description', 50);
            $table->integer('height');
            $table->integer('length');
            $table->integer('width');
            $table->integer('weight');
            $table->boolean('stackable');
            $table->boolean('carrier');
            $table->boolean('flatbed');
            $table->bigInteger('third_id')->unsigned();
            $table->bigInteger('status_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->foreign('third_id')->references('id')->on('third')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('status_id')->references('id')->on('product_status')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('category_id')->references('id')->on('product_category')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function(Blueprint $table) {
            $table->dropForeign('product_status_id_foreign');
        });

        Schema::table('product', function(Blueprint $table) {
            $table->dropForeign('product_category_id_foreign');
        });

        Schema::table('product', function(Blueprint $table) {
            $table->dropForeign('product_third_id_foreign');
        });

        Schema::dropIfExists('product');
    }
}
