<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('reference', 20);
            $table->float('volume');
            $table->float('weight');
            $table->float('ad_valorem');
            $table->integer('ot')->nullable();
            $table->integer('contract_week');
            $table->integer('wish_week')->nullable();
            $table->integer('year');
            $table->integer('delivery_date')->nullable();
            $table->string('note', 255)->nullable();
            $table->float('price');
            $table->float('country_price');
            $table->float('island_price');
            $table->float('special_price');
            $table->float('against_cash')->nullable();
            $table->bigInteger('departure_site_id')->unsigned();
            $table->bigInteger('destination_site_id')->unsigned();
            $table->bigInteger('type_id')->unsigned();
            $table->bigInteger('third_id')->unsigned();
            $table->bigInteger('status_id')->unsigned();
            $table->foreign('departure_site_id')->references('id')->on('site')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('destination_site_id')->references('id')->on('site')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('type_id')->references('id')->on('order_type')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('third_id')->references('id')->on('third')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('status_id')->references('id')->on('order_status')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function(Blueprint $table) {
            $table->dropForeign('order_departure_site_id_foreign');
        });

        Schema::table('order', function(Blueprint $table) {
            $table->dropForeign('order_destination_site_id_foreign');
        });

        Schema::table('order', function(Blueprint $table) {
            $table->dropForeign('order_type_id_foreign');
        });

        Schema::table('order', function(Blueprint $table) {
            $table->dropForeign('order_status_id_foreign');
        });

        Schema::table('order', function(Blueprint $table) {
            $table->dropForeign('order_third_id_foreign');
        });

        Schema::dropIfExists('order');
    }
}
