<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('order')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('restrict')->onUpdate('restrict');
            $table->string("observation",50);
            $table->integer("quantity");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product', function(Blueprint $table) {
            $table->dropForeign('order_product_order_id_foreign');
        });
        Schema::table('order_products', function(Blueprint $table) {
            $table->dropForeign('order_product_product_id_foreign');
        });
        Schema::dropIfExists('order_products');
    }
}
