<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ThirdTypeSeeder::class);
        $this->call(ThirdStatusSeeder::class);
        $this->call(ThirdSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(ProductStatusSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(OrderTypeSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(SiteSeeder::class);
    }
}
