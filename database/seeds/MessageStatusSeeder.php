<?php

use Illuminate\Database\Seeder;

class MessageStatusSeeder extends Seeder {
    public function run()
    {
        DB::table('message_status')->delete();

        DB::table('message_status')->insert([
            'status' => 'Plannification'
        ]);

        DB::table('message_status')->insert([
            'status' => 'Chargement'
        ]);
    }
}
