<?php

use Illuminate\Database\Seeder;

class MessageTypeSeeder extends Seeder {
    public function run()
    {
        DB::table('message_type')->delete();

        DB::table('message_type')->insert([
            'type' => 'Avancée'
        ]);

        DB::table('message_type')->insert([
            'type' => "Communication"
        ]);
    }
}
