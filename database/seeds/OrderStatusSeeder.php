<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder {
    public function run()
    {
        DB::table('order_status')->delete();

        DB::table('order_status')->insert([
            'status' => 'Prise en compte'
        ]);

        DB::table('order_status')->insert([
            'status' => "Groupée"
        ]);

        DB::table('order_status')->insert([
            'status' => "En livraison"
        ]);

        DB::table('order_status')->insert([
            'status' => "Livrée"
        ]);
    }
}
