<?php

use Illuminate\Database\Seeder;

class OrderTypeSeeder extends Seeder {
    public function run()
    {
        DB::table('order_type')->delete();

        DB::table('order_type')->insert([
            'type' => 'LAD'
        ]);

        DB::table('order_type')->insert([
            'type' => "LDM"
        ]);
    }
}
