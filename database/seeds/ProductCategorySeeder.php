<?php

use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder {
    public function run()
    {
        DB::table('product_category')->delete();

        DB::table('product_category')->insert([
            'category' => 'Abris'
        ]);

        DB::table('product_category')->insert([
            'category' => 'Piscine'
        ]);
    }
}
