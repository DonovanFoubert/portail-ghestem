<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder {
    public function run()
    {
        DB::table('product')->delete();


        for($i = 1; $i <= 10; ++$i) {
            DB::table('product')->insert([
                'ean' => '12345678912'.$i,
                'reference' => 'ABR00'.$i,
                'description' => 'Petit abris'.$i,
                'height' => '100',
                'length' => '200',
                'width' => '120',
                'weight' => '400',
                'stackable' => true,
                'carrier' => true,
                'flatbed' => false,
                'third_id' => '3',
                'status_id' => '1',
                'category_id' => '1'
            ]);

            DB::table('product')->insert([
                'ean' => '12345678913'.$i,
                'reference' => 'PIS00'.$i,
                'description' => 'Petite piscine',
                'height' => '300',
                'length' => '600',
                'width' => '140',
                'weight' => '200',
                'stackable' => false,
                'carrier' => false,
                'flatbed' => true,
                'third_id' => '2',
                'status_id' => '2',
                'category_id' => '2'
            ]);
        }
    }
}
