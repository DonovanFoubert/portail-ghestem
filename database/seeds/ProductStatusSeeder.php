<?php

use Illuminate\Database\Seeder;

class ProductStatusSeeder extends Seeder {
    public function run()
    {
        DB::table('product_status')->delete();

        DB::table('product_status')->insert([
            'status' => 'Actif'
        ]);

        DB::table('product_status')->insert([
            'status' => 'Inactif'
        ]);
    }
}
