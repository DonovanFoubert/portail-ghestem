<?php

use Illuminate\Database\Seeder;

class SiteSeeder extends Seeder {
    public function run()
    {
        DB::table('site')->delete();

        DB::table('site')->insert([
            'name' => 'Houplines',
            'address' => 'Rue des tests 1',
            'postal_code' => '59000',
            'city' => 'Lille',
            'country' => 'France',
            'third_id' => '1'
        ]);

        DB::table('site')->insert([
            'name' => 'LCA',
            'address' => 'Rue des tests 2',
            'postal_code' => '59000',
            'city' => 'Lille',
            'country' => 'France',
            'third_id' => '1'
        ]);

        DB::table('site')->insert([
            'name' => 'SIFAS',
            'address' => 'Rue des tests 3',
            'postal_code' => '59000',
            'city' => 'Lille',
            'country' => 'France',
            'third_id' => '1'
        ]);
    }
}
