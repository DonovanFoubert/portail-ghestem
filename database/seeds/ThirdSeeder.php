<?php

use Illuminate\Database\Seeder;

class ThirdSeeder extends Seeder {
    public function run()
    {
        DB::table('third')->delete();

        DB::table('third')->insert([
            'name' => 'Ghestem',
            'address' => 'Rue des tests',
            'postal_code' => '59000',
            'city' => 'Lille',
            'country' => 'France',
            'status_id' => '1',
            'type_id' => '1'
        ]);

        DB::table('third')->insert([
            'name' => 'Leroy Merlin',
            'address' => 'Avenue pouf',
            'postal_code' => '75000',
            'city' => 'Paris',
            'country' => 'France',
            'status_id' => '1',
            'type_id' => '2'
        ]);

        DB::table('third')->insert([
            'name' => 'Gardenas',
            'address' => 'Avenue Louise',
            'postal_code' => '1000',
            'city' => 'Bruxelles',
            'country' => 'Belgique',
            'status_id' => '2',
            'type_id' => '2'
        ]);
    }
}
