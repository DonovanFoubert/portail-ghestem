<?php

use Illuminate\Database\Seeder;

class ThirdStatusSeeder extends Seeder {
    public function run()
    {
        DB::table('third_status')->delete();

        DB::table('third_status')->insert([
            'status' => 'Actif'
        ]);

        DB::table('third_status')->insert([
            'status' => 'Inactif'
        ]);
    }
}
