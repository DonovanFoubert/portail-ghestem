<?php

use Illuminate\Database\Seeder;

class ThirdTypeSeeder extends Seeder {
    public function run()
    {
        DB::table('third_type')->delete();

        DB::table('third_type')->insert([
            'type' => 'Gestionnaire'
        ]);

        DB::table('third_type')->insert([
            'type' => "Donner d'ordre"
        ]);
    }
}
