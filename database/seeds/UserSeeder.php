<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name' => 'Donovan',
            'email' => 'donovan@prograticweb.be',
            'password' => bcrypt('Mdp1234*')
        ]);
    }
}
