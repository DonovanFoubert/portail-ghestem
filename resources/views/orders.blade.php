@extends('template')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Commande #{{ $order->id}}</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Exporter</a>
</div>

<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_gen" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Informations générales</h6>
                <div class="dropdown no-arrow" id="drop_gen">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div id="bloc_gen" style="display: none;" class="col-12 row">
                <div class="col-4">
                    {!! Form::label('third', 'Fournisseur : ') !!}
                    {!! Form::text('third', $order->third->name, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('reference', 'Référence : ') !!}
                    {!! Form::text('reference', $order->reference, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('type', 'Type : ') !!}
                    {!! Form::text('type', $order->type->type, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('status', 'Statut : ') !!}
                    {!! Form::text('status', $order->status->status, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('volume', 'Volume : ') !!}
                    {!! Form::text('volume', $order->volume, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('weight', 'Poids : ') !!}
                    {!! Form::text('weight', $order->weight, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('ot', 'OT : ') !!}
                    {!! Form::text('ot', $order->ot, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('contract_week', 'Semaine Contractuelle : ') !!}
                    {!! Form::text('contract_week', $order->contract_week, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('wish_week', 'Semaine Souhaitée : ') !!}
                    {!! Form::text('wish_week', $order->wish_week, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('year', 'Année : ') !!}
                    {!! Form::text('year', $order->year, array('readonly')) !!}
                </div>
                <div class="col-4">
                    {!! Form::label('note', 'Observation : ') !!}
                    {!! Form::textarea('note', $order->note, array('readonly', 'cols' => '30')) !!}
                </div>
            </div>
        </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_dest" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Point de destination</h6>
                <div class="dropdown no-arrow" id="drop_dest">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body" id="bloc_dest" style="display: none;">
                <div class="col-12 row">
                    <div class="col-4">
                    {!! Form::label('name', 'Nom : ') !!}
                    {!! Form::text('name', $order->destination_site->name, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('address', 'Adresse : ') !!}
                    {!! Form::text('address', $order->destination_site->address, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('postal_code', 'Code Postal : ') !!}
                    {!! Form::text('postal_code', $order->destination_site->postal_code, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('city', 'Ville : ') !!}
                    {!! Form::text('city', $order->destination_site->city, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('country', 'Pays : ') !!}
                    {!! Form::text('country', $order->destination_site->country, array('readonly')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_map" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Carte</h6>
                <div class="dropdown no-arrow" id="drop_map">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body" id="bloc_map" style="display: none;">
                <div class="col-12 row">
                    <div class="col-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d7302.638790155887!2d2.905308164115462!3d50.68297515951788!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sHouplines%20zac%20des%20moulins%20de%20la%20lys!5e0!3m2!1sfr!2sfr!4v1597731329308!5m2!1sfr!2sfr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_finance" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Financier</h6>
                <div class="dropdown no-arrow" id="drop_finance">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body" id="bloc_finance" style="display: none;">
                <div class="col-12 row">
                    <div class="col-4">
                    {!! Form::label('ad_valorem', 'Ad Valorem : ') !!}
                    {!! Form::text('ad_valorem', $order->ad_valorem, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('against_cash', 'Contre Remboursement : ') !!}
                    {!! Form::text('against_cash', $order->against_cash, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('price', 'Tarif Calculé : ') !!}
                    {!! Form::text('price', $order->price, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('country_price', 'Tarif Pays : ') !!}
                    {!! Form::text('country_price', $order->country_price, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('island_price', 'Tarif Ile : ') !!}
                    {!! Form::text('island_price', $order->island_price, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('special_price', 'Tarif Spécial : ') !!}
                    {!! Form::text('special_price', $order->special_price, array('readonly')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Area Chart -->
    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_start" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Point de départ</h6>
                <div class="dropdown no-arrow" id="drop_start">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body" id="bloc_start" style="display: none;">
                <div class="col-12 row">
                    <div class="col-4">
                    {!! Form::label('name', 'Nom : ') !!}
                    {!! Form::text('name', $order->departure_site->name, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('address', 'Adresse : ') !!}
                    {!! Form::text('address', $order->departure_site->address, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('postal_code', 'Code Postal : ') !!}
                    {!! Form::text('postal_code', $order->departure_site->postal_code, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('city', 'Ville : ') !!}
                    {!! Form::text('city', $order->departure_site->city, array('readonly')) !!}
                    </div>
                    <div class="col-4">
                    {!! Form::label('country', 'Pays : ') !!}
                    {!! Form::text('country', $order->departure_site->country, array('readonly')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_message" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Messages</h6>
                <div class="dropdown no-arrow" id="drop_message">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body" id="bloc_message" style="display: none;">
                <div class="col-12 p-3">
                    <!-- Card Body -->
                    @if(isset($order->messages) && count($order->messages) > 0)
                        @foreach($order->messages as $message)
                            <p>{{ $message->created_at }} - {{ $message->type->type }} - {{ $message->user->name }} : {{ $message->message }}</p>
                        @endforeach
                    @else
                        <p>Aucun message pour cette commande</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" id="click_product" style="cursor: pointer">
                <h6 class="m-0 font-weight-bold text-primary">Produits</h6>
                <div class="dropdown no-arrow" id="drop_product">
                    <i class="fas fa-plus fa-sm fa-fw text-gray-400" style="cursor: pointer"></i>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body" id="bloc_product" style="display: none;">
                <div class="col-12 p-3 row text-center">
                    <!-- Card Body -->
                    <div class="demo-container">
                        <div id="gridContainer"></div>
                    </div>
<!--
                    @if(isset($order->products) && count($order->products) > 0)
                        <p class="col-2 border-right"></p>
                        <p class="col-2 border-bottom border-right">Référence</p>
                        <p class="col-2 border-bottom border-right">Description</p>
                        <p class="col-2 border-bottom border-right">Quantité</p>
                        <p class="col-2 border-bottom border-right">Observation</p>
                        <p class="col-2"></p>
                        @foreach($order->products as $product)
                            <p class="col-2 border-right"></p>
                            <p class="col-2 border-bottom border-right">{{ $product->reference }}</p>
                            <p class="col-2 border-bottom border-right">{{ $product->description }}</p>
                            <p class="col-2 border-bottom border-right">{{ $product->pivot->quantity }}</p>
                            <p class="col-2 border-bottom border-right">{{ $product->pivot->observation }}</p>
                            <p class="col-2"></p>
                        @endforeach
                    @else
                        <p>Aucun produit pour cette commande</p>
                    @endif
-->
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $( "#click_gen " ).click(function() {
        if (!$( "#bloc_gen" ).is(':visible')) {
            $( "#bloc_gen" ).fadeIn();
            $( "#drop_gen i" ).removeClass("fa-plus");
            $( "#drop_gen i" ).addClass("fa-minus");
        } else {
            $( "#bloc_gen" ).fadeOut();
            $( "#drop_gen i" ).removeClass("fa-minus");
            $( "#drop_gen i" ).addClass("fa-plus");
        }
    });

    $( "#click_dest" ).click(function() {
        if (!$( "#bloc_dest" ).is(':visible')) {
            $( "#bloc_dest" ).fadeIn();
            $( "#drop_dest i" ).removeClass("fa-plus");
            $( "#drop_dest i" ).addClass("fa-minus");
        } else {
            $( "#bloc_dest" ).fadeOut();
            $( "#drop_dest i" ).removeClass("fa-minus");
            $( "#drop_dest i" ).addClass("fa-plus");
        }
    });

    $( "#click_map" ).click(function() {
        if (!$( "#bloc_map" ).is(':visible')) {
            $( "#bloc_map" ).fadeIn();
            $( "#drop_map i" ).removeClass("fa-plus");
            $( "#drop_map i" ).addClass("fa-minus");
        } else {
            $( "#bloc_map" ).fadeOut();
            $( "#drop_map i" ).removeClass("fa-minus");
            $( "#drop_map i" ).addClass("fa-plus");
        }
    });

    $( "#click_finance" ).click(function() {
        if (!$( "#bloc_finance" ).is(':visible')) {
            $( "#bloc_finance" ).fadeIn();
            $( "#drop_finance i" ).removeClass("fa-plus");
            $( "#drop_finance i" ).addClass("fa-minus");
        } else {
            $( "#bloc_finance" ).fadeOut();
            $( "#drop_finance i" ).removeClass("fa-minus");
            $( "#drop_finance i" ).addClass("fa-plus");
        }
    });

    $( "#click_start" ).click(function() {
        if (!$( "#bloc_start" ).is(':visible')) {
            $( "#bloc_start" ).fadeIn();
            $( "#drop_start i" ).removeClass("fa-plus");
            $( "#drop_start i" ).addClass("fa-minus");
        } else {
            $( "#bloc_start" ).fadeOut();
            $( "#drop_start i" ).removeClass("fa-minus");
            $( "#drop_start i" ).addClass("fa-plus");
        }
    });

    $( "#click_message" ).click(function() {
        if (!$( "#bloc_message" ).is(':visible')) {
            $( "#bloc_message" ).fadeIn();
            $( "#drop_message i" ).removeClass("fa-plus");
            $( "#drop_message i" ).addClass("fa-minus");
        } else {
            $( "#bloc_message" ).fadeOut();
            $( "#drop_message i" ).removeClass("fa-minus");
            $( "#drop_message i" ).addClass("fa-plus");
        }
    });

    DevExpress.localization.locale(navigator.language || navigator.browserLanguage);

    $( "#click_product" ).click(function() {
        if (!$( "#bloc_product" ).is(':visible')) {
            $( "#bloc_product" ).fadeIn();
            $( "#drop_product i" ).removeClass("fa-plus");
            $( "#drop_product i" ).addClass("fa-minus");

            var products;

            $.ajax({
                url:  '{{ url("/commandes/articles/".$order->id)}}',
                dataType: "json",
                async: false,
                success: function(msg){
                    products = msg;
                }
            });

            $(function() {
                var dataGrid = $("#gridContainer").dxDataGrid({
                    dataSource: {
                        store: {
                            type: "array",
                            data: products
                        }
                    },
                    paging: {
                        pageSize: 10
                    },
                    editing: {
                        mode: "row",
                        allowDeleting: true,
                        allowAdding: true
                    },
                    pager: {
                        showPageSizeSelector: false,
                        allowedPageSizes: [100]
                    },
                    remoteOperations: false,
                    grouping: {
                        autoExpandAll: false
                    },
                    rowAlternationEnabled: true,
                    showBorders: true,
                    columns: [
                        {dataField: "reference", caption: "Référence", dataType: "string"},
                        {dataField: "description", caption: "Description", dataType: "string"},
                        {dataField: "quantity", caption: "Quantité", dataType: "number"},
                        {dataField: "observation", caption: "Observation", dataType: "string"}
                    ]
                });
            });
        } else {
            $( "#bloc_product" ).fadeOut();
            $( "#drop_product i" ).removeClass("fa-minus");
            $( "#drop_product i" ).addClass("fa-plus");
        }
    });
</script>
@endsection
