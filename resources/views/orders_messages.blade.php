@extends('template')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Commande #{{ $order->id}}</h1>
    <div class="col-xl-2 col-md-6">
        <div class="card border-left-light shadow h-100 py-2">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <a href="{{ asset("/commandes/1") }}" style="text-decoration: none;"><div class="text-xs font-weight-bold text-secondary text-uppercase mb-1 text-center">Infos générales</div></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <a href="{{ asset("/commandes/1/messages") }}" style="text-decoration: none;"><div class="text-xs font-weight-bold text-premiary text-uppercase mb-1 text-center">Messages</div></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6">
        <div class="card border-left-light shadow h-100 py-2">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <a href="#" style="text-decoration: none;"><div class="text-xs font-weight-bold text-secondary text-uppercase mb-1 text-center">Produits</div></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6">
        <div class="card border-left-light shadow h-100 py-2">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <a href="#" style="text-decoration: none;"><div class="text-xs font-weight-bold text-secondary text-uppercase mb-1 text-center">Formulaire</div></a>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Exporter</a>
</div>

<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Informations générales</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="col-12 p-3">
                    {!! Form::label('third', 'Fournisseur : ') !!}
                    {!! Form::text('third', $order->third->name, array('readonly')) !!}<br>

                    {!! Form::label('reference', 'Référence : ') !!}
                    {!! Form::text('reference', $order->reference, array('readonly')) !!}<br>

                    {!! Form::label('type', 'Type : ') !!}
                    {!! Form::text('type', $order->type->type, array('readonly')) !!}<br>

                    {!! Form::label('status', 'Statut : ') !!}
                    {!! Form::text('status', $order->status->status, array('readonly')) !!}<br>

                    {!! Form::label('volume', 'Volume : ') !!}
                    {!! Form::text('volume', $order->volume, array('readonly')) !!}<br>

                    {!! Form::label('weight', 'Poids : ') !!}
                    {!! Form::text('weight', $order->weight, array('readonly')) !!}<br>

                    {!! Form::label('ot', 'OT : ') !!}
                    {!! Form::text('ot', $order->ot, array('readonly')) !!}<br>

                    {!! Form::label('contract_week', 'Semaine Contractuelle : ') !!}
                    {!! Form::text('contract_week', $order->contract_week, array('readonly')) !!}<br>

                    {!! Form::label('wish_week', 'Semaine Souhaitée : ') !!}
                    {!! Form::text('wish_week', $order->wish_week, array('readonly')) !!}<br>

                    {!! Form::label('year', 'Année : ') !!}
                    {!! Form::text('year', $order->year, array('readonly')) !!}<br>

                    {!! Form::label('note', 'Observation : ') !!}
                    {!! Form::textarea('note', $order->note, array('readonly', 'cols' => '40')) !!}<br>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Messages</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="col-12 p-3">
                    <!-- Card Body -->
                    @if(isset($order->messages) && count($order->messages) > 0)
                        @foreach($order->messages as $message)
                            <p>{{ $message->created_at }} - {{ $message->user->name }} : {{ $message->message }}</p>
                        @endforeach
                    @else
                        <p>Aucun message pour cette commande</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
