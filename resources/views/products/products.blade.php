@extends('template')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Articles</h1>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-8 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <span class="text-xs font-weight-bold text-primary text-uppercase mb-1">Filtres</span>
                        Fournisseur
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>Exporter les données</a>
</div>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3" id="success" style="display: none; position: absolute; z-index: 2000;">
        <div class="card border-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Modification effectuée!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3" id="delete" style="display: none; position: absolute; z-index: 2000;">
        <div class="card border-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Suppression effectuée!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3" id="error" style="display: none; position: absolute; z-index: 2000;">
        <div class="card border-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">L'opération a échoué !!!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->

<div class="row">

    <div class="demo-container">
        <div id="gridContainer"></div>
    </div>

</div>

<script>
    DevExpress.localization.locale(navigator.language || navigator.browserLanguage);

    var products;
    var thirds;
    var productStatus;
    var productCategory;

    $.ajax({
        url:  '{{ url("/articles/liste")}}',
        dataType: "json",
        async: false,
        success: function(data){
            products = data;
        }
    });

    $.ajax({
        url:  '{{ url("/tiers/liste")}}',
        dataType: "json",
        async: false,
        success: function(data){
            thirds = data;
        }
    });

    $.ajax({
        url:  '{{ url("/articles/statuts/liste")}}',
        dataType: "json",
        async: false,
        success: function(msg){
            productStatus = msg;
        }
    });

    $.ajax({
        url:  '{{ url("/articles/categories/liste")}}',
        dataType: "json",
        async: false,
        success: function(msg){
            productCategory = msg;
        }
    });

    $(function() {
        var dataGrid = $("#gridContainer").dxDataGrid({
            dataSource: {
                store: {
                    type: "array",
                    data: products,
                    key: "id"
                }
            },
            paging: {
                pageSize: 10
            },
            editing: {
                mode: "row",
                allowUpdating: true,
                allowDeleting: true,
                allowAdding: true
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 25, 50, 100]
            },
            remoteOperations: false,
            searchPanel: {
                visible: true,
                highlightCaseSensitive: true
            },
            groupPanel: { visible: true },
            grouping: {
                autoExpandAll: false
            },
            sorting: {
                mode: "multiple"
            },
            allowColumnReordering: true,
            rowAlternationEnabled: true,
            filterRow: {
                visible: true
            },
            showBorders: true,
            columns: [
                {dataField: "ean", caption: "EAN", dataType: "string"},
                {dataField: "reference", caption: "Référence", dataType: "string"},
                {dataField: "description", caption: "Description", dataType: "string"},
                {dataField: "height", caption: "Hauteur", dataType: "number"},
                {dataField: "length", caption: "Longeur", dataType: "number"},
                {dataField: "width", caption: "Largeur", dataType: "number"},
                {dataField: "weight", caption: "Poids", dataType: "number"},
                {dataField: "stackable", caption: "Gerbable", dataType: "boolean"},
                {dataField: "carrier", caption: "Porteur", dataType: "boolean"},
                {dataField: "flatbed", caption: "Plateau", dataType: "boolean"},
                {dataField: "third", caption: "Fournisseur", dataType: "number", lookup: {dataSource: { store: {
                                type: "array",
                                data: thirds,
                                key: "id"
                            }}, displayExpr: "name", valueExpr: "id"}},
                {dataField: "status", caption: "Statut", dataType: "number", lookup: {dataSource: { store: {
                                                                                                type: "array",
                                                                                                data: productStatus,
                                                                                                key: "id"
                                                                                        }}, displayExpr: "status", valueExpr: "id"}},
                {dataField: "category", caption: "Catégorie", dataType: "number", lookup: {dataSource: { store: {
                                                                                                type: "array",
                                                                                                data: productCategory,
                                                                                                key: "id"
                                                                                            }}, displayExpr: "category", valueExpr: "id"}}
            ],
            summary: {
            groupItems: [{
                column: "ean",
                summaryType: "count",
                alignByColumn: true
            }]},
            onRowUpdating: function(e) {
                url = "{{ url("/articles/update")}}";
                data = {"id":e.key,"newData":e.newData};
                $.ajax({
                    type: "POST",
                    beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));},
                    url: url,
                    data: data,
                    success: function(){
                        $("#gridContainer").dxDataGrid("instance").refresh();
                        $("#success").center();
                        $("#success").show().delay(3000).queue(function(n) {
                            $(this).hide(); n();
                        });
                    },
                    onFailure: function(){
                        $("#error").center();
                        $("#error").show().delay(3000).queue(function(n) {
                            $(this).hide(); n();
                        });
                    }
                });
            },
            onRowRemoving: function(e) {
                url = "{{ url("/articles")}}/"+e.key;
                $.ajax({
                    type: "DELETE",
                    beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));},
                    url: url,
                    success: function(){
                        $(".dx-dialog").hide();
                        $("#gridContainer").dxDataGrid("instance").refresh();
                        $("#delete").center();
                        $("#delete").show().delay(3000).queue(function(n) {
                            $(this).hide(); n();
                        });
                    },
                    onFailure: function(){
                        $("#error").center();
                        $("#error").show().delay(3000).queue(function(n) {
                            $(this).hide(); n();
                        });
                    }
                });
            },
            onRowInserting: function(e) {
                console.log(e);
                url = "{{ url("/articles/new")}}";
                data = {"newData":e.data};
                $.ajax({
                    type: "POST",
                    data: data,
                    beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));},
                    url: url,
                    success: function(){
                        $("#gridContainer").dxDataGrid("instance").refresh();
                        $("#success").center();
                        $("#success").show().delay(3000).queue(function(n) {
                            $(this).hide(); n();
                        });
                    },
                    onFailure: function(){
                        $("#error").center();
                        $("#error").show().delay(3000).queue(function(n) {
                            $(this).hide(); n();
                        });
                    }
                });
            }
        });
    });

    var collapsed = false;

    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
            $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
            $(window).scrollLeft()) + "px");
        return this;
    }
</script>

@endsection
