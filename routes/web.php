<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name("home");

Route::get('/commandes', 'OrderController@index');
Route::get('/commandes/creation', 'OrderController@create');

Route::get('/articles', 'ProductController@index');
Route::get('/articles/creation', 'ProductController@create');
Route::get('/articles/liste', 'ProductController@liste');
Route::post("/articles/update", 'ProductController@edit');
Route::post("/articles/new", 'ProductController@create');
Route::delete("/articles/{id}", 'ProductController@destroy');

Route::get('/articles/statuts/liste', 'ProductStatusController@liste');

Route::get('/articles/categories/liste', 'ProductCategoryController@liste');

Route::get('/tiers', 'ThirdController@index');
Route::get('/tiers/creation', 'ThirdController@create');
Route::get('/tiers/liste', 'ThirdController@liste');
Route::get('/tiers/listeForSite', 'ThirdController@listeForSite');
Route::post("/tiers/update", 'ThirdController@edit');
Route::post("/tiers/new", 'ThirdController@create');
Route::delete("/tiers/{id}", 'ThirdController@destroy');

Route::get('/tiers/statuts/liste', 'ThirdStatusController@liste');

Route::get('/tiers/categories/liste', 'ThirdTypeController@liste');

Route::get('/sites', 'SiteController@index');
Route::get('/sites/creation', 'SiteController@create');
Route::get('/sites/liste', 'SiteController@liste');
Route::post("/sites/update", 'SiteController@edit');
Route::post("/sites/new", 'SiteController@create');
Route::delete("/sites/{id}", 'SiteController@destroy');

Route::get('/commandes/creation', 'OrderController@index');
Route::get('/commandes/{id}/messages', 'OrderController@messages');
Route::get('/commandes/{id}', 'OrderController@detail');
Route::get("/commandes/articles/{id}", 'OrderController@listeProducts');
